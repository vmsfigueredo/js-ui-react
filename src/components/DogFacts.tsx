import React, {useState} from 'react';
import Grid from "@mui/material/Grid";
import {MenuItem, Pagination, TextField} from "@mui/material";
import Title from "./Title";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";

interface DogFact {
    id: number,
    fact: string
}

type DogFactsType = {
    data: DogFact[];
    current_page: number;
    first_page: number;
    last_page: number;
    per_page: number;
} | null;

const DogFacts = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [pageSize, setPageSize] = useState(25);
    const [dogFacts, setDogFacts] = useState<DogFactsType>(null);
    const handleChangePage = (evt: any, newPage: any) => {
        setCurrentPage(newPage);
    }
    const fetchData = async () => {
        try {
            const response = await fetch(`http://localhost:8042/api/v1/dog-facts?page=${currentPage}&pageSize=${pageSize}`);
            const jsonData = await response.json();
            setDogFacts(jsonData);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };
    React.useEffect(() => {
        fetchData();
    }, [currentPage, pageSize]);
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} md={4}><Pagination count={dogFacts?.last_page ?? 0} page={currentPage}
                                                  onChange={handleChangePage}/></Grid>
            <Grid item xs={12} md={4}>
                <Title>Dog Facts</Title>
            </Grid>
            <Grid item xs={12} md={4} display={'flex'} justifyContent={'flex-end'}>
                <TextField select
                           size={'small'}
                           label={'Facts Per Page'}
                           value={pageSize}
                           onChange={(evt) => {
                               setPageSize(parseInt(evt.target.value))
                           }}>
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={25}>25</MenuItem>
                    <MenuItem value={50}>50</MenuItem>
                    <MenuItem value={100}>100</MenuItem>
                </TextField>
            </Grid>
            <Grid item xs={12}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>Fact</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dogFacts?.data.map((fact) => (
                            <TableRow key={`fact-${fact.id}`}>
                                <TableCell>{fact.id}</TableCell>
                                <TableCell>{fact.fact}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Grid>
        </Grid>
    )
}

export default DogFacts
