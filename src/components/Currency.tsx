import React from 'react';
import Title from "./Title";
import Typography from "@mui/material/Typography";

interface CurrencyDataInterface {
    code: string,
    description: string,
    rate: string,
    rate_float: number,
    symbol: string
}

type CurrencyType = {
    data: CurrencyDataInterface,
    disclaimer: string,
    time: string
}

const decodeHTMLEntity = (text: string) => {
    const doc = new DOMParser().parseFromString(text, 'text/html');
    return doc.documentElement.textContent;
}
const Currency = ({data, disclaimer, time}: CurrencyType) => {
    return (
        <React.Fragment>
            <Typography color="text.secondary">
                {time}
            </Typography>
            <div>
                <Title>1 BTC to {data.code}</Title>
                <Typography component="p" variant="h4">
                    {decodeHTMLEntity(data.symbol)}{' '}
                    {data.rate_float.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    })}
                </Typography>

            </div>
            <Typography color="text.secondary" variant={'caption'}>
                {disclaimer}
            </Typography>
        </React.Fragment>
    );
}

export default Currency;
