import React from "react";

import Grid from "@mui/material/Grid";
import Currency from "../components/Currency";
import {Card} from "@mui/material";

export default function BitcoinContent() {
    const [bitcoinData, setBitcoinData] = React.useState<any>({})
    React.useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://api.coindesk.com/v1/bpi/currentprice.json');
                const jsonData = await response.json();
                setBitcoinData(jsonData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    return (
        <Grid container spacing={3}>
            {bitcoinData.bpi && Object.keys(bitcoinData.bpi).map((key: string, index: number) => (
                <Grid key={index} item xs={12} md={4}>
                    <Card variant={'outlined'} sx={{
                        p: 2,
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        height: 240
                    }}>
                        <Currency data={bitcoinData.bpi?.[key]} time={bitcoinData.time.updated}
                                  disclaimer={bitcoinData.disclaimer}/>
                    </Card>
                </Grid>
            ))}
        </Grid>
    );
}

