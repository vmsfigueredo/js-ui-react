import React, {useContext} from "react";

interface DashboardProviderInterface {
    children: React.ReactNode;
}

export type DashboardContextType = {
    currentPage: string;
    pageTitle: string;
    setPageInfo: Function;
}

const DashboardContext = React.createContext<DashboardContextType | null>(null);

export const useDashboardContext = () => useContext(DashboardContext);
const DashboardProvider = ({children}: DashboardProviderInterface) => {
    const [currentPage, setCurrentPage] = React.useState('dashboard');
    const [pageTitle, setPageTitle] = React.useState('Dashboard');
    const setPageInfo = (newPage: string, newPageTitle: string) => {
        setCurrentPage(newPage);
        setPageTitle(newPageTitle);
    }
    return (
        <DashboardContext.Provider value={
            {
                currentPage, pageTitle, setPageInfo
            }
        }>
            {children}
        </DashboardContext.Provider>
    )
}

export default DashboardProvider;
