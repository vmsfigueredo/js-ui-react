import React from 'react';
import {render, screen} from '@testing-library/react';
import Orders from '../components/Orders';
import Deposits from '../components/Deposits';
import Currency from "../components/Currency";

test('renders orders', () => {
    render(<Orders/>);
    const title = screen.getByText(/recent orders/i);
    expect(title).toBeInTheDocument();
});

test('renders deposits', () => {
    render(<Deposits/>);
    const title = screen.getByText(/recent deposits/i);
    expect(title).toBeInTheDocument();
});

test('renders currency', () => {
    const data = {
        code: 'USD',
        description: 'US Dollar',
        rate: '1,234.56',
        rate_float: 1234.56,
        symbol: '&#36;'
    };
    const disclaimer = 'This is a disclaimer.';
    const time = '2023-08-05T02:47:00+00:00';

    const {getByText} = render(<Currency data={data} disclaimer={disclaimer} time={time}/>);

    // Check if the time is rendered correctly
    const timeElement = getByText(/2023-08-05T02:47:00\+00:00/);
    expect(timeElement).toBeInTheDocument();

    // Check if the currency title is rendered correctly
    const titleElement = getByText(/1 BTC to USD/);
    expect(titleElement).toBeInTheDocument();

    // Check if the currency rate is rendered correctly with the correct format
    const rateElement = getByText(/\$ 1,234.56/);
    expect(rateElement).toBeInTheDocument();

    // Check if the disclaimer is rendered correctly
    const disclaimerElement = getByText(/This is a disclaimer./);
    expect(disclaimerElement).toBeInTheDocument();
});
