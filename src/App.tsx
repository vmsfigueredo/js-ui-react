import React from 'react';
import './App.css';
import DashboardProvider from "./context/DashboardContext";
import Dashboard from "./Dashboard";

function App() {
    return (
        <div className="App">
            <DashboardProvider>
                <Dashboard/>
            </DashboardProvider>
        </div>
    );
}

export default App;
